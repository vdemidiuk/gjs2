import React from 'react';
import PropTypes from 'prop-types';

import List from 'components/List';
import ListItem from 'components/ListItem';
import LoadingIndicator from 'components/LoadingIndicator';
import JobsListItem from 'components/JobsListItem';


const JobsList = ({ loading, error, jobs }) => {
  if (loading) {
    return <List component={LoadingIndicator} />;
  }

  if (error !== false) {
    const ErrorComponent = () => (
      <ListItem item={'Something went wrong, please try again!'} />
    );
    return <List component={ErrorComponent} />;
  }

  if (!loading && jobs.length === 0) {
    const NoticeComponent = () => (
      <ListItem item={'Nothing found!'} />
    );
    return <List component={NoticeComponent} />;
  }

  if (jobs !== false) {
    return <List items={jobs} component={JobsListItem} />;
  }

  return null;
};

JobsList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  jobs: PropTypes.any
};

export default JobsList;
