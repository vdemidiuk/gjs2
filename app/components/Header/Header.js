import React from 'react';
// import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import './style.scss';

import SimpleLayout from '../SimpleLayout';

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <AppBar position="static" style={ {backgroundColor: '#1d80be'} }>
        <Toolbar>
          <SimpleLayout>
            <Typography variant="h6" color="inherit">
              GitHub Jobs
            </Typography>
          </SimpleLayout>
        </Toolbar>
      </AppBar>
      // <div className="header">
      //   <div className="nav-bar">
      //     <Link className="router-link" to="/">
      //       Home
      //     </Link>
      //   </div>
      // </div>
    );
  }
}

export default Header;
