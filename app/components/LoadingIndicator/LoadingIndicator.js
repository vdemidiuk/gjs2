import React from 'react';
import './style.scss';
import CircularProgress from '@material-ui/core/CircularProgress';

const LoadingIndicator = () => (
  <div className="loading-indicator">
    <CircularProgress/>
  </div>
);

export default LoadingIndicator;
