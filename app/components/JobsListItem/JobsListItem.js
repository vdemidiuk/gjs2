
import React from 'react';
import PropTypes from 'prop-types';
import ListItem from 'components/ListItem';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import './style.scss';
import { compose } from 'redux';

import withWidth from '@material-ui/core/withWidth';

class JobsListItem extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { item } = this.props;
    const isMobile = this.props.width === 'xs' || this.props.width === 'sm' 
    // console.log('item', item)
    const content = (
      <Grid container className="jobs-list-item">
        <Grid item xs={12} md={8}>
          <Typography variant="subtitle2"><a href={item.url} target="_blank">{item.title}</a></Typography>
          <Typography><span className="grey">{item.company}</span> - <span className="fulltime">{item.type}</span></Typography>
        </Grid>
        <Grid item xs={12} md={4} align={isMobile ? "left" : "right"}>
          <Typography>{item.location}</Typography>
          <Typography className="grey">{moment(item.created_at).fromNow()}</Typography>
        </Grid>
      </Grid>
    );

    return (
      <ListItem key={`jobs-list-item-${item.full_name}`} item={content} />
    );
  }
}

export default compose(withWidth())(JobsListItem);

JobsListItem.propTypes = {
  item: PropTypes.object,
};
