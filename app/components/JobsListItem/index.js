import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
// import { makeSelectCurrentUser } from 'containers/App/selectors';
import JobsListItem from './JobsListItem';

export default connect(
  createStructuredSelector({
    // currentUser: makeSelectCurrentUser()
  })
)(JobsListItem);
