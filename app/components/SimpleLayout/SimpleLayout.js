import React from 'react';
import PropTypes from 'prop-types';
import Hidden from '@material-ui/core/Hidden';
import Grid from '@material-ui/core/Grid';
import './style.scss';

const SimpleLayout = ({children}) => (
  <React.Fragment>
    <Hidden smDown>
      <Grid item md={1} lg={2}>&nbsp;</Grid>
    </Hidden>
    <Grid item xs={12} md={10} lg={8}>
      {children}
    </Grid>
    <Hidden smDown>
      <Grid item md={1} lg={2}>&nbsp;</Grid>
    </Hidden>
  </React.Fragment>
);

SimpleLayout.propTypes = {
  children: PropTypes.any,
};


export default SimpleLayout;
