/**
 * SeachPage selectors
 */

import { createSelector } from 'reselect';

const selectSearch = (state) => state.get('search');

const makeSelectDescription = () => createSelector(
  selectSearch,
  (searchState) => searchState.get('description')
);

const makeSelectLocation = () => createSelector(
  selectSearch,
  (searchState) => searchState.get('location')
);

const makeSelectFulltime = () => createSelector(
  selectSearch,
  (searchState) => searchState.get('fulltime')
);

export {
  selectSearch,
  makeSelectDescription,
  makeSelectFulltime,
  makeSelectLocation
};
