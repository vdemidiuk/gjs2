import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import withWidth from '@material-ui/core/withWidth';
import {
  makeSelectJobs,
  makeSelectLoading,
  makeSelectError
} from 'containers/App/selectors';
import { loadJobs } from '../App/actions';
import { changeDescription, changeLocation, changeFulltime } from './actions';
import { makeSelectDescription, makeSelectFulltime, makeSelectLocation } from './selectors';
import reducer from './reducer';
import saga from './saga';
import SearchPage from './SearchPage';

const mapDispatchToProps = (dispatch) => ({
  onChangeDescription: (evt) => dispatch(changeDescription(evt.target.value)),
  onChangeLocation: (evt) => dispatch(changeLocation(evt.target.value)),
  onChangeFulltime: (evt) => { dispatch(changeFulltime(evt.target.checked)) },
  onSubmitForm: (evt) => {
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    dispatch(loadJobs());
  }
});

const mapStateToProps = createStructuredSelector({
  jobs: makeSelectJobs(),
  description: makeSelectDescription(),
  location: makeSelectLocation(),
  fulltime: makeSelectFulltime(),
  loading: makeSelectLoading(),
  error: makeSelectError()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'search', reducer });
const withSaga = injectSaga({ key: 'search', saga });

export default compose(withReducer, withSaga, withConnect)(SearchPage);
export { mapDispatchToProps };
