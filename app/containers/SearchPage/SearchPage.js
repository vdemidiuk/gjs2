/*
 * SearchPage
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";

import JobsList from "components/JobsList";
import SimpleLayout from "components/SimpleLayout";


import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import "./style.scss";

export default class SearchPage extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.submitForm = this.submitForm.bind(this);
  }

  componentDidMount() {
    const search = this.constructor.parseUriSearch(this.props.history.location.search);

    if (search) {
      this.props.onChangeDescription({ target: { value: search.description } });
      this.props.onChangeLocation({ target: { value: search.location } });
      this.props.onChangeFulltime({
        target: { checked: search.fulltime === "true" ? true : false }
      });
      this.submitForm();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.jobs !== this.props.jobs) {
    }
  }

  static parseUriSearch(search) {
    const parsed = search.substring(1).split("/");
    if (parsed.length < 3) {
      return false;
    }
    return {
      description: parsed[0],
      location: parsed[1],
      fulltime: parsed[2]
    };
  }

  submitForm() {
    this.props.history.push({
      pathname: "/",
      search: `${this.props.description}/${this.props.location}/${
        this.props.fulltime
      }`
    });
    this.props.onSubmitForm();
  }

  render() {
    const {
      loading,
      error,
      jobs,
      description,
      location,
      fulltime,
      onChangeDescription,
      onChangeFulltime,
      onChangeLocation,
      onSubmitForm
    } = this.props;


    const jobsListProps = {
      loading,
      error,
      jobs
    };

    return (
      <React.Fragment>
        <Helmet>
          <title>Jobs</title>
        </Helmet>
        <div className="search-page">
          {/* <form onSubmit={onSubmitForm}> */}
          <Grid container>
            <SimpleLayout>
              <Grid container spacing={16}>
                <Grid item xs={12} md={4} lg={4}>
                  <TextField
                    id="description"
                    label="Description"
                    value={description}
                    onChange={onChangeDescription}
                    margin="normal"
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12} md={4} lg={4}>
                  <TextField
                    id="location"
                    label="Location"
                    value={location}
                    onChange={onChangeLocation}
                    margin="normal"
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12} md={4} lg={4}>
                  <FormGroup row className="form-group-row">
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={fulltime}
                          onChange={onChangeFulltime}
                          value="true"
                        />
                      }
                      label="Full Time Only"
                    />
                    <Button
                      className="form-group-btn"
                      variant="contained"
                      onClick={this.submitForm}
                    >
                      Search
                    </Button>
                  </FormGroup>
                </Grid>
              </Grid>

              <JobsList {...jobsListProps} />
            </SimpleLayout>
          </Grid>
          {/* </form> */}
        </div>
      </React.Fragment>
    );
  }
}

SearchPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  jobs: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  description: PropTypes.string,
  location: PropTypes.string,
  fulltime: PropTypes.bool,
  onChangeDescription: PropTypes.func,
  onChangeLocation: PropTypes.func,
  onChangeFulltime: PropTypes.func
};
