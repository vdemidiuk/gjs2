/*
 * searchReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import { CHANGE_FULLTIME, CHANGE_LOCATION, CHANGE_DESCRIPTION } from './constants';

// The initial state of the App
const initialState = fromJS({
  location: '',
  description: '',
  fulltime: false,
});

function searchReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_DESCRIPTION:
      return state.set('description', action.description);
    case CHANGE_LOCATION:
      return state.set('location', action.location);
    case CHANGE_FULLTIME:
      return state.set('fulltime', action.fulltime);
    default:
      return state;
  }
}

export default searchReducer;
