

import {
  call, put, select, takeLatest
} from 'redux-saga/effects';
import { LOAD_JOBS } from 'containers/App/constants';
import { jobsLoaded, jobsLoadingError } from 'containers/App/actions';

import request from 'utils/request';
import { makeSelectDescription, makeSelectFulltime, makeSelectLocation } from 'containers/SearchPage/selectors';

export function* getJobs() {

  const description = yield select(makeSelectDescription());
  const location = yield select(makeSelectLocation());
  const fulltime = yield select(makeSelectFulltime());
  const requestURL = `https://cors-anywhere.herokuapp.com/https://jobs.github.com/positions.json?description=${description}&location=${location}&full_time=${fulltime}`;

  try {
    const jobs = yield call(request, requestURL);
    yield put(jobsLoaded(jobs));
  } catch (err) {
    console.log(err)
    yield put(jobsLoadingError(err));
  }
}

export default function* githubData() {
  yield takeLatest(LOAD_JOBS, getJobs);
}
