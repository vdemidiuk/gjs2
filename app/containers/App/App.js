/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route } from 'react-router-dom';
// import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import SearchPage from 'containers/SearchPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Header from 'components/Header';
import Footer from 'components/Footer';
import './style.scss';

// const theme = createMuiTheme({
//   palette: {
//     primary: {
//       main: '#2B7FC3',
//     },
//     secondary: {
//       main: '#869ca9',
//     }
//   },
//   typography: {
//     useNextVariants: true,
//   },
// });

const App = () => (
  <div className="app-wrapper">
    <Helmet
      titleTemplate="%s - GitHub Jobs Search"
      defaultTitle="GitHub Jobs Search"
    >
      <meta name="description" content="GitHub Jobs Search application" />
    </Helmet>
    <Header />
    {/* <MuiThemeProvider theme={theme}> */}
      <Switch>
        <Route exact path="/" component={SearchPage} />
        <Route path="" component={NotFoundPage} />
      </Switch>
    {/* </MuiThemeProvider> */}
    <Footer />
  </div>
);

export default App;
