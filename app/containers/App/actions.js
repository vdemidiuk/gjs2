/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  LOAD_JOBS,
  LOAD_JOBS_SUCCESS,
  LOAD_JOBS_ERROR,
} from './constants';

export function loadJobs() {
  return {
    type: LOAD_JOBS,
  };
}

export function jobsLoaded(jobs) {
  return {
    type: LOAD_JOBS_SUCCESS,
    jobs,
  };
}

export function jobsLoadingError(error) {
  return {
    type: LOAD_JOBS_ERROR,
    error,
  };
}
